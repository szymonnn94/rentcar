<?php
/**
 * Car
 */
namespace RentCarBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Car
 *
 * @ORM\Table(name="car")
 * @ORM\Entity(repositoryClass="RentCarBundle\Repository\CarRepository")
 */
class Car
{
    /**
    *car id
     * @var int
     *
     * @ORM\Column(name="car_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
    * brand
     * @var string
     *
     * @ORM\Column(name="brand", type="string", length=100)
     */
    private $brand;

    /**
    * model
     * @var string
     *
     * @ORM\Column(name="model", type="string", length=100)
     */
    private $model;

    /**
    * class
     * @var string
     *
     * @ORM\Column(name="class", type="string", length=5)
     */
    private $class;

    /**
    * img url
     * @var string
     *
     * @ORM\Column(name="img", type="string", length=255)
     */
    private $img;

    /**
    * price for 1 day
     * @var int
     *
     * @ORM\Column(name="priceFor1Day", type="integer")
     */
    private $priceFor1Day;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set brand
     *
     * @param string $brand
     *
     * @return Car
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * Get brand
     *
     * @return string
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Set model
     *
     * @param string $model
     *
     * @return Car
     */
    public function setModel($model)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * Get model
     *
     * @return string
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set class
     *
     * @param string $class
     *
     * @return Car
     */
    public function setClass($class)
    {
        $this->class = $class;

        return $this;
    }

    /**
     * Get class
     *
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

        /**
     * Set img
     *
     * @param string $img
     *
     * @return Car
     */
    public function setImg($img)
    {
        $this->img = $img;

        return $this;
    }

    /**
     * Get img
     *
     * @return string
     */
    public function getImg()
    {
        return $this->img;
    }

    /**
     * Set priceFor1Day
     *
     * @param integer $priceFor1Day
     *
     * @return Car
     */
    public function setPriceFor1Day($priceFor1Day)
    {
        $this->priceFor1Day = $priceFor1Day;

        return $this;
    }

    /**
     * Get priceFor1Day
     *
     * @return int
     */
    public function getPriceFor1Day()
    {
        return $this->priceFor1Day;
    }
}

