<?php
/**
 * CarOrder entity
 */

namespace RentCarBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;

/**
 * CarOrder entity
 *
 * @ORM\Table(name="car_order")
 * @ORM\Entity(repositoryClass="RentCarBundle\Repository\CarOrderRepository")
 */
class CarOrder
{
    /**
     * CarOrder id
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */

    private $id;

    /**
    * CarOrder user id
     * @var int
     * @ORM\Column(name="rent_user", type="integer")
     * @ORM\OneToMany(targetEntity="User", mappedBy="rent_user")
     */
    private $rentUser;

    /**
    * CarOrder car id
     * @var int
     * @ORM\Column(name="car_id", type="integer")
     * @ORM\OneToMany(targetEntity="Car", mappedBy="car_id")
     *
     */
    private $carId;

    /**
    * CarOrder datetime
     * @var \DateTime
     *
     * @ORM\Column(name="order_date", type="datetime")
     */
    private $orderDate;

    /**
    * CarOrder return datetime
     * @var \DateTime
     *
     * @ORM\Column(name="order_return_date", type="datetime", nullable=true)
     */
    private $orderReturnDate;

    /**
    * CarOrder expected return datetime
     * @var \DateTime
     *
     * @ORM\Column(name="order_predicted_return_date", type="datetime")
     */
    private $orderPredictedReturnDate;

    /**
    * CarOrder opinion
     * @var string
     *
     * @ORM\Column(name="opinion", type="string", length=255, nullable=true)
     */
    private $opinion;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rentUser
     *
     * @param integer $rentUser
     *
     * @return CarOrder
     */
    public function setRentUser($rentUser)
    {
        $this->rentUser = $rentUser;

        return $this;
    }

    /**
     * Get rentUser
     *
     * @return int
     */
    public function getRentUser()
    {
        return $this->rentUser;
    }

    /**
     * Set carId
     *
     * @param integer $carId
     *
     * @return CarOrder
     */
    public function setCarId($carId)
    {
        $this->carId = $carId;

        return $this;
    }

    /**
     * Get carId
     *
     * @return int
     */
    public function getCarId()
    {
        return $this->carId;
    }

    /**
     * Set orderDate
     *
     * @param \DateTime $orderDate
     *
     * @return CarOrder
     */
    public function setOrderDate($orderDate)
    {
        $this->orderDate = $orderDate;

        return $this;
    }

    /**
     * Get orderDate
     *
     * @return \DateTime
     */
    public function getOrderDate()
    {
        return $this->orderDate;
    }

    /**
     * Set orderReturnDate
     *
     * @param \DateTime $orderReturnDate
     *
     * @return CarOrder
     */
    public function setOrderReturnDate($orderReturnDate)
    {
        $this->orderReturnDate = $orderReturnDate;

        return $this;
    }

    /**
     * Get orderReturnDate
     *
     * @return \DateTime
     */
    public function getOrderReturnDate()
    {
        return $this->orderReturnDate;
    }

    /**
     * Set orderPredictedReturnDate
     *
     * @param \DateTime $orderPredictedReturnDate
     *
     * @return CarOrder
     */
    public function setOrderPredictedReturnDate($orderPredictedReturnDate)
    {
        $this->orderPredictedReturnDate = $orderPredictedReturnDate;

        return $this;
    }

    /**
     * Get orderPredictedReturnDate
     *
     * @return \DateTime
     */
    public function getOrderPredictedReturnDate()
    {
        return $this->orderPredictedReturnDate;
    }

    /**
     * Set opinion
     *
     * @param string $opinion
     *
     * @return CarOrder
     */
    public function setOpinion($opinion)
    {
        $this->opinion = $opinion;

        return $this;
    }

    /**
     * Get opinion
     *
     * @return string
     */
    public function getOpinion()
    {
        return $this->opinion;
    }
}

