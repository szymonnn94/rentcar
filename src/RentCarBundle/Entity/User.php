<?php
/**
* User
*/
namespace RentCarBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class represents User
 * @ORM\Entity
 * @ORM\Table(name="rent_user")
 */
class User extends BaseUser
{
    /**
     * user id
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
    * construtor
    */
    public function __construct()
    {
        parent::__construct();
        // your own logic
    }
}