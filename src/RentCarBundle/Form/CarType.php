<?php
    /**
     *CarType
     */
namespace RentCarBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

    /**
     *CarType
     */
class CarType extends AbstractType
{
    /**
    * build form
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('brand')
            ->add('model')
            ->add('class')
            ->add('img')
            ->add('priceFor1Day')
        ;
    }
    
    /**
    * configure options
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'RentCarBundle\Entity\Car'
        ));
    }
}
