<?php
/**
 * OpinionType
 */
namespace RentCarBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * OpinionType
 */
class OpinionType extends AbstractType
{
    /**
    * build form
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            // ->add('rentUser')
            // ->add('carId')
    //          ->add('orderDate', 'datetime', array(
    // 'data' => new \DateTime()))
            // ->add('orderReturnDate', 'datetime')
    //         ->add('orderPredictedReturnDate', 'datetime', array(
    // 'data' => new \DateTime()))
             ->add('opinion')
        ;
    }
    
    /**
    * configure options
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'RentCarBundle\Entity\CarOrder'
        ));
    }
}
