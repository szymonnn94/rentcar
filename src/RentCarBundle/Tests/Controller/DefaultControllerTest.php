<?php
/**
* DefaultControllerTest
*/
namespace RentCarBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
* DefaultControllerTest
*/
class DefaultControllerTest extends WebTestCase
{
	/**
* test index
*/
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $this->assertContains('Hello World', $client->getResponse()->getContent());
    }
}
