<?php
/**
* DefaultController
*/
namespace RentCarBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use RentCarBundle\Entity\Car;
use RentCarBundle\Entity\CarOrder;
use RentCarBundle\Form\OpinionType;
use RentCarBundle\Form\CarOrderMainType;
use Symfony\Component\HttpFoundation\Request;

/**
* DefaultController
*/
class DefaultController extends Controller
{
    /**
* index page
*/
    public function indexAction()
    {
    	// $car = new Car();
    	// $car -> setBrand("Volkswagen");
    	// $car -> setModel("Passat");
    	// $car -> setClass("B");
    	// $car -> setPriceFor1Day(150);

    	$em = $this->container->get('doctrine')->getManager();
    	$repo = $em->getRepository('RentCarBundle:Car');
    	$qb = $em->createQueryBuilder();
        $qb->select('c.brand', 'c.model', 'c.class', 'c.img', 'c.priceFor1Day','count(c.id) AS cnt')
            ->from('RentCarBundle\Entity\Car', 'c')
            ->leftJoin('RentCarBundle\Entity\CarOrder', 'o','WITH', 'o.carId = c.id')
            ->where('o.id is null or o.orderReturnDate is not null')
            ->groupBy('c.img','c.brand', 'c.model', 'c.class', 'c.priceFor1Day');
        $query = $qb->getQuery();
        $cars = $query->getResult();

        $qb = $em->createQueryBuilder();
        $qb->select('c.brand', 'c.model', 'c.class', 'c.img', 'COUNT(o.id) AS cnt')
            ->from('RentCarBundle\Entity\CarOrder', 'o')
            ->innerJoin('RentCarBundle\Entity\Car', 'c','WITH', 'o.carId = c.id')
            ->where('o.opinion IS NOT NULL')
            ->groupBy('c.img','c.brand', 'c.model', 'c.class')
            ->setMaxResults(3)
            ->orderBy('cnt', 'desc');
        $query = $qb->getQuery();
        $topRated = $query->getResult();

        $qb = $em->createQueryBuilder();
        $qb->select('c.brand','c.model', 'c.class', 'c.img', 'COUNT(c.id) AS cnt')
            ->from('RentCarBundle\Entity\CarOrder', 'o')
            ->innerJoin('RentCarBundle\Entity\Car', 'c','WITH', 'o.carId = c.id')
            ->groupBy('c.brand', 'c.model', 'c.class', 'c.img')
            ->setMaxResults(3)
            ->orderBy('cnt', 'desc');
        $query = $qb->getQuery();
        $topRented = $query->getResult();

        return $this->render('RentCarBundle:Default:carlist.html.twig', array(
            'cars'=> $cars, 'topRated' => $topRated, 'topRented' => $topRented,
        ));
    }

/**
* transaction history page
*/
    public function historyAction()
    {

        $user = $this->container->get('security.context')->getToken()->getUser();
        $em = $this->container->get('doctrine')->getManager();

        $orderRepo = $em->getRepository('RentCarBundle:CarOrder');
        $carRepo = $em->getRepository('RentCarBundle:Car');

        //$orders = $orderRepo->findBy(array('rentUser' => $user->getId()));
        //$cars = $carRepo->findBy(array('carId' => $order->getCarId()));
        $qb = $em->createQueryBuilder();
        $qb->select('o.id','o.carId', 'o.rentUser', 'o.orderDate', 'o.orderPredictedReturnDate', 'o.orderReturnDate', 'o.opinion', 'c.brand', 'c.model', 'c.priceFor1Day', 'c.img')
            ->from('RentCarBundle\Entity\CarOrder', 'o')
            ->where('o.rentUser='.$user->getId())
            ->innerJoin('RentCarBundle\Entity\Car', 'c','WITH', 'o.carId = c.id');
        $query = $qb->getQuery();
        $orders = $query->getResult();
        return $this->render('RentCarBundle:Default:history.html.twig', array('orders' => $orders));
    }

/**
* order page
* @param String $carBrand brandname of ordering car
* @param String $carModel model of ordering car
* @param Request $request request from form
*/
    public function orderAction($carBrand, $carModel, Request $request)
    {
        $em = $this->container->get('doctrine')->getManager();
        $repo = $em->getRepository('RentCarBundle:Car');
        $qb = $em->createQueryBuilder();
        $qb->select('c')
            ->from('RentCarBundle\Entity\Car', 'c')
            ->leftJoin('RentCarBundle\Entity\CarOrder', 'o','WITH', 'o.carId = c.id')
            ->where("(o.id is null or o.orderReturnDate is not null) and c.brand='".$carBrand."' and c.model='".$carModel."'")
            ->setMaxResults(1);
        $query = $qb->getQuery();
        $car = $query->getArrayResult();

        $carOrder = new CarOrder();
        $user = $this->container->get('security.context')->getToken()->getUser();
        $carOrder -> setRentUser($user->getId());
        $form = $this->createForm(CarOrderMainType::class, $carOrder);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $orderDate = $form['orderDate']->getData();
            $orderPredictedReturnDate = $form['orderPredictedReturnDate']->getData();
            $carOrder -> setOrderDate($orderDate);
            $carOrder -> setOrderReturnDate(null);
            $carOrder -> setOrderPredictedReturnDate($orderPredictedReturnDate);
            $carOrder -> setCarId($car[0]['id']);
            $em = $this->getDoctrine()->getManager();
            $em->persist($carOrder);
            $em->flush();
            $cost = (($orderPredictedReturnDate->getTimestamp() - $orderDate->getTimestamp())/86400) * $car[0]['priceFor1Day'];
            return $this->render('RentCarBundle:Default:confirm.html.twig', array('car' => $car, 'carOrder' => $carOrder, 'cost' => $cost));
    }
    return $this->render('RentCarBundle:Default:order.html.twig', array('form' => $form->createView(), 'car' => $car));
    }
/**
* opinion page
* @param Request $request from form
* @param int $orderId order id which is rated
*/
    public function opinionAction(Request $request, $orderId)
    {
        $em = $this->container->get('doctrine')->getManager();
        $repo = $em->getRepository('RentCarBundle:CarOrder');
        $order = $repo->findOneBy(array('id' => $orderId));

        $form = $this->createForm(OpinionType::class, $order);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $opinion = $form['opinion']->getData();
            $order->setOpinion($opinion);
            $em = $this->getDoctrine()->getManager();
            $em->merge($order);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
        'notice',
        'Dziękujemy za wyrażenie swojej opinii!'
    );
            //return $this->redirectToRoute('rent_car_homepage');
    }
    return $this->render('RentCarBundle:Default:opinion.html.twig', array('form' => $form->createView(), 'order' => $order));
    }
}

