<?php

/**
 * CarOrder controller.
 *
 */
namespace RentCarBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use RentCarBundle\Entity\CarOrder;
use RentCarBundle\Form\CarOrderType;

/**
 * CarOrder controller.
 *
 */
class CarOrderController extends Controller
{
    /**
     * Lists all CarOrder entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $carOrders = $em->getRepository('RentCarBundle:CarOrder')->findAll();

        return $this->render('carorder/index.html.twig', array(
            'carOrders' => $carOrders,
        ));
    }

    /**
     * Creates a new CarOrder entity.
     * @param Request request from form
     */
    public function newAction(Request $request)
    {
        $carOrder = new CarOrder();
        $form = $this->createForm('RentCarBundle\Form\CarOrderType', $carOrder);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($carOrder);
            $em->flush();

            return $this->redirectToRoute('carorder_show', array('id' => $carorder->getId()));
        }

        return $this->render('carorder/new.html.twig', array(
            'carOrder' => $carOrder,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a CarOrder entity.
     * @param CarOrder record to show
     */
    public function showAction(CarOrder $carOrder)
    {
        $deleteForm = $this->createDeleteForm($carOrder);

        return $this->render('carorder/show.html.twig', array(
            'carOrder' => $carOrder,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing CarOrder entity.
     * @param Request request from form
     * @param CarOrder record to edit
     */
    public function editAction(Request $request, CarOrder $carOrder)
    {
        $deleteForm = $this->createDeleteForm($carOrder);
        $editForm = $this->createForm('RentCarBundle\Form\CarOrderType', $carOrder);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($carOrder);
            $em->flush();

            return $this->redirectToRoute('carorder_edit', array('id' => $carOrder->getId()));
        }

        return $this->render('carorder/edit.html.twig', array(
            'carOrder' => $carOrder,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a CarOrder entity.
     * @param Request request from form
     * @param CarOrder record to delete
     */
    public function deleteAction(Request $request, CarOrder $carOrder)
    {
        $form = $this->createDeleteForm($carOrder);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($carOrder);
            $em->flush();
        }

        return $this->redirectToRoute('carorder_index');
    }

    /**
     * Creates a form to delete a CarOrder entity.
     *
     * @param CarOrder $carOrder The CarOrder entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(CarOrder $carOrder)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('carorder_delete', array('id' => $carOrder->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}

